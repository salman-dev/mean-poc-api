
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var mongoUri = "mongodb://salman.khan:Sal%40DBPOC30@ds025232.mlab.com:25232/mean-poc-db";

// configure app to use bodyParser()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Set Port
var port = process.env.PORT || 3010;

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'Welcome to our POC API!' }); 
});

//	CREATE Payment
router.post('/payment', function(req, res) {
	MongoClient.connect(mongoUri, function(err, db) {
		if (err) {
			res.send({message:"Error in saving data"})
		}
		console.log("Connected to Database");

		//insert record
		db.collection('payment').insert(req.body, function(err, records) {
			if (err) {
				res.send({message:"Error in saving data"})
			}
			res.send({message:"Payment saved successfully"})
		});
	});
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('POC API running on port: ' + port);





