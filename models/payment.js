var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var PaymentSchema   = new Schema({
    paymentId: String,
    paymentState: String,
    userId: String,
    userName: String,
    userAddress: String,
    productId: String,
    productPrice: Number,
    commission: Number
});

module.exports = mongoose.model('Payment', PaymentSchema);